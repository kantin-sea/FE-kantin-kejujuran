import { BrowserRouter as Router } from 'react-router-dom';
import React from 'react'
import { Layout } from './routes/Layout.js';
import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient()

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
          <Layout/>
      </Router>
    </QueryClientProvider>
  );
}

export default App;
