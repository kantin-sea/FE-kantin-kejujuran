import Navbar from 'react-bootstrap/Navbar'
import {Container, Nav} from 'react-bootstrap'
import './Navbar.css'
import React from 'react'

const PageNavbar = () =>{
    return(
        <Navbar collapseOnSelect  expand="lg"  className='navbar-wrapper'>
            <Container>
                <Navbar.Brand href="/">Kantin Kejujuran</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link href="/list-produk">Lihat Produk</Nav.Link>
                    <Nav.Link href="/tambah-produk">Tambah Produk</Nav.Link>
                    <Nav.Link href="/balance">Balance Box</Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default PageNavbar;