import React from 'react'
import {ApplicationRoutes} from './ApplicationRoutes'
import PageNavbar  from '../components/Navbar/Navbar'
export const Layout = () => {
     return(
          <>
               <PageNavbar />
               <ApplicationRoutes/>
               {/**Footer (if needed)**/}
          </>
     )
}