import {Routes,Route} from 'react-router-dom'
import { ListProductPage } from '../pages/product/ListProductPage'
import { TambahProductPage } from '../pages/product/TambahProductPage'
import { BalancePage } from '../pages/box/BallancePage'


export const ApplicationRoutes = () => {
     return(
          <Routes>
               <Route path='/' element={<ListProductPage/>}/>
               <Route path='/list-produk' element={<ListProductPage/>}/>
               <Route path='/tambah-produk' element={<TambahProductPage/>}/>
               <Route path='/balance' element={<BalancePage/>}/>
          
          </Routes>
     )
}