import axios from 'axios'
import React, {useEffect, useState} from 'react'
import { Container, Row, Col, Tabs, Tab, Form, Button } from 'react-bootstrap'



export const BalancePage = () => {
     const [currentTab, setCurrentTab] = React.useState('top-up')
     const [nominalTopUp, setNominalTopUp] = useState(0)
     const [nominalWithdraw, setNominalWithdraw] = useState(0)
     const [refetch, setRefetch] = useState(true)
     const [currentBalance, setCurrentBalance] = useState()
  
     useEffect (() => {
          const fetchSaldo = async () => {
               let config = {
                    url: 'https://kantin-kejujuran-sea.herokuapp.com/api/balance-box/balance/',
                    method: 'get',
                    }
                    axios(config).then((res) => {

                         console.log(res)
                        setCurrentBalance(res.data.saldo)
                        setRefetch(false)
                    }).catch(() =>{
                        alert('Error when fetch saldo')
                    })
          }

          if(refetch){
               fetchSaldo()
           }

     }, [currentBalance, refetch])

     const TopUp  = async () => {
          var dataSend = new FormData();
          dataSend.append('nominal', nominalTopUp);
          
          let config = {
          url: 'https://kantin-kejujuran-sea.herokuapp.com/api/balance-box/top-up/',
          method: 'put',
          data: dataSend,
          }
  
          axios(config).then((res) => {
              alert('Top up success')
              window.location.reload()
          }).catch(() =>{
              alert('Error when top-up')
          })
      }

      const withdraw  = async () => {
          var dataSend = new FormData();
          dataSend.append('nominal', nominalWithdraw);
          
          let config = {
          url: 'https://kantin-kejujuran-sea.herokuapp.com/api/balance-box/withdraw/',
          method: 'put',
          data: dataSend,
          }
  
          axios(config).then((res) => {
              alert('Withdraw Success')
              window.location.reload()
          }).catch((e) =>{
               console.log(e)
              alert('Cannot withdraw more than current balance')
          })
      }
  
      const handleSubmitTopUp = e => {
          e.preventDefault();
          TopUp();
      }

      const handleSubmitWithdraw = e => {
          e.preventDefault();
          withdraw()
      }


     return(
          <>
               <Container className='py-3'>
                    <div className='text-center my-5'>
                         <h1>Canteen Ballance Box</h1>
                         <h4>Current Balance: Rp. {currentBalance}</h4>
                    </div>
                    <Row>
                         <Col className={`py-3`}>
                              <Tabs
                                   activeKey={currentTab}
                                   onSelect={(tab) => setCurrentTab(tab)}
                                   className="mb-3"
                                   fill 
                                   justify
                              >
                                   <Tab eventKey="top-up" title="Top up">
                                        <Form onSubmit={handleSubmitTopUp}>
                                             <Form.Group className="mb-3" controlId="formBasicNama">
                                                  <Form.Label>Nominal Top Up (unlimited)</Form.Label>
                                                  <Form.Control type="text" name="name"  onChange={e => setNominalTopUp(e.target.value)} placeholder='Dalam Rupiah' required/>
                                             </Form.Group>
                                             <Button className='btn btn-light button-1' type="submit">
                                                  Top Up
                                             </Button>
                                        </Form>
                                   </Tab>
                                   <Tab eventKey="withdraw" title="Withdraw">
                                        <Form onSubmit={handleSubmitWithdraw}>
                                             <Form.Group className="mb-3" controlId="formBasicNama">
                                                  <Form.Label>Nominal Withdraw (maksimum:{currentBalance})</Form.Label>
                                                  <Form.Control type="text" name="name"  onChange={e => setNominalWithdraw(e.target.value)} placeholder='Dalam Rupiah' required/>
                                                  <Button className='btn btn-light button-1 mt-3' type="submit">
                                                   Withdraw
                                                  </Button>
                                             </Form.Group>
                                        </Form>
                                   </Tab>
                              </Tabs>
                         </Col>
                    </Row>
               </Container>
          </>
     )
}