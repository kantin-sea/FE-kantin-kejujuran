import {Card, Button, Row, Col, Container, Form } from 'react-bootstrap'
import React, {useEffect, useState} from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import '../../App.css'

export const TambahProductPage = () =>{
    let navigate = useNavigate()
    const [formData, setFormData] = useState({
        name:"",
        desc:"",
        price:"",
        image:'',
    })


    const createProduk  = async () => {
       
        var dataSend = new FormData();

        for(var key in formData){
            dataSend.append(key, formData[key]);
        }
        let config = {
        url: 'https://kantin-kejujuran-sea.herokuapp.com/api/store/produk/',
        method: 'post',
        data: dataSend,
        }

        axios(config).then((res) => {
            alert('Produk berhasil ditambahkan')
            navigate('/list-produk')
        }).catch(() =>{
            alert('Error when add produk')
        })
    }

    const handleSubmit = e => {
        console.log(formData)
        e.preventDefault();
        createProduk();
    }

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });
    const handleChangeFile = (event) => {
        setFormData({ ...formData, image: event.target.files[0] });
    }

    return(
        <Container>
            <Form onSubmit={handleSubmit} className ='m-5 shadow p-5'>
                <Form.Group className="mb-3" controlId="formBasicNama">
                    <Form.Label>Nama</Form.Label>
                    <Form.Control type="text" name="name"  onChange={e => onChange(e)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicHarga">
                    <Form.Label>Harga</Form.Label>
                    <Form.Control type="text"   name="price" onChange={e => onChange(e)} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicHarga">
                    <Form.Label>Deskripsi</Form.Label>
                    <Form.Control as="textarea" rows={3}  name="desc" onChange={e => onChange(e)}/>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Foto produk</Form.Label>
                    <Form.Control   type="file" name='image' onChange={handleChangeFile} />
                </Form.Group>
                <Button className='btn btn-light button-1' type="submit">
                    Submit
                </Button>
            </Form>
        </Container>
        )
}