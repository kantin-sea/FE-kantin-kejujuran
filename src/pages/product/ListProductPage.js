import {Card, Button, Row, Col, Container, Form } from 'react-bootstrap'
import React, {useEffect, useState} from 'react'
import axios from 'axios'
import '../../App.css'

export const ListProductPage = () =>{

    const [daftarProduk, setDaftarProduk] = useState([])
    const [refetch, setRefetch] = useState(true)
    const [sortBy, setSortBy] = useState('date')
    const [type, setType] = useState('dsc')

    useEffect(() => {
        if(refetch){
            sortDataProduk()
            setRefetch(false)
        }
    }, [daftarProduk, refetch])

    const beliProduk  = async (id) => {
        console.log(id)
        let config = {
        url: `https://kantin-kejujuran-sea.herokuapp.com/api/store/beli/${id}`,
        method: 'delete',
        }
        axios(config).then((res) => {
            alert('Produk Berhasil Dibeli')
            window.location.reload(); 
        }).catch(() =>{
            alert('Error when buy produk')
        })
    }

    const sortDataProduk  = async () => {
        let config = {
        url: `https://kantin-kejujuran-sea.herokuapp.com/api/store/sortProduk/${sortBy}`,
        method: 'get',
        }
        axios(config).then((res) => {
            let dataProduk = res.data
            if (type === 'asc'){
                dataProduk = dataProduk.reverse()
            }
            setDaftarProduk(dataProduk)
            
        }).catch(() =>{
            alert('Error when fetch produk')
        })
    }

    const handleSubmit = e => {
        console.log('hai')
        e.preventDefault();
        sortDataProduk()
    }

    return(
        <Container>
            <h1 className='text-center my-5'>List Produk</h1>
            <Form onSubmit={handleSubmit} className="mb-4">
                <Row>
                <Col md={3}>
                    <Form.Select value={sortBy} onChange={e => setSortBy(e.target.value)}>
                        <option value="date">Date Time Created</option>
                        <option value="name">Product Name</option>
                    </Form.Select>
                </Col>
                <Col md={3}>
                    <Form.Check
                        inline
                        label="Ascending"
                        name="group1"
                        type='radio'
                        value='asc'
                        checked={type === 'asc'}
                        onChange={e => setType(e.target.value)}
                    />
                    <Form.Check
                        inline
                        label="Descending"
                        name="group1"
                        type='radio'
                        value='dsc'
                        checked={type === 'dsc'}
                        onChange={e => setType(e.target.value)}
                    />
                </Col>
                <Col ><Button className='btn btn-dark button-1 btn-sm' style={{width:'180px'}} type="submit">
                    Sort
                </Button></Col>
                </Row>
            </Form>
            <Row>
            {daftarProduk.length != 0 ? daftarProduk.map(item =>(
                <Col>
                    <Card style={{ width: '22rem',  borderRadius:'2rem', backgroundColor:'whitesmoke' }} className="mb-3 shadow">
                    <Card.Img variant="top" src={item.image} style={{ height: '14rem', borderRadius:'2rem' }} />
                    <Card.Body>
                        <Card.Text style={{ fontSize:'18px'}}>{item.name}</Card.Text>
                        <Card.Text style={{ fontSize:'17px', fontWeight: 'bold' }}>
                        Rp.{item.price}
                        </Card.Text>
                        <Card.Text style={{ fontSize:'17px', fontWeight: 'bold' }}>
                        {item.desc}
                        </Card.Text>
                        <Card.Text style={{ fontSize:'12px', fontWeight: 'bold' }}>
                        {item.timestamp}
                        </Card.Text>
                        <Button className='btn btn-light button-1' onClick={() => beliProduk(item.id)}>Beli Produk</Button>
                    </Card.Body>
                    </Card>
                </Col>

            )) : <h1 className='text-center'>Belum ada Produk</h1>};
            </Row>
        </Container>
        )
}